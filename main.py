import os 
import sys
import json 
from tabulate import tabulate
from nltk.tokenize import sent_tokenize,RegexpTokenizer
import nltk 


nltk.download('punkt')

def word_norm(word):
    for char in '-.,\n":':
        word = word.replace(char,' ')
    word = word.lower()
    return word
    

def save_data(data, json_name):
    with open(json_name, 'w') as json_file:
        json.dump(data,json_file)
    return json_name
    
class WordsCounter:
    def __init__(self, txt_file):
        self.txt_file = txt_file
        self.total_words = 0
        self.word_freq = dict()
        self._counting()
        # self.json_result
        self.count_lst 
        self.sentence_lst
        self.sen_group = []
        self._word_in_sentence()
        # self.final_lst 

    def _counting(self):
        with open (self.txt_file, 'r') as f: ### READ THE TEXT FILE 
            para = f.read()

        ### SENTENCE SEGMENTATION
        self.sentence_lst = sent_tokenize(para)

        ### WORD SEGMENTATION
        for word_i in word_norm(para).split():
            word_i = word_norm(word_i)
            self.word_freq.setdefault(word_i, 0)
            self.word_freq[word_i] += 1
            self.total_words += 1

        self.json_result = save_data(self.word_freq,"result1.txt")
        self.count_lst = [(k, v) for k, v in self.word_freq.items()] 
        self.count_lst.sort(key=lambda x:x[1], reverse=True)


    def _word_in_sentence(self):
        self.final_lst = []
        # print (self.final_lst)
       
        print ('-----------------------------------------------\n' +
        "filename: " +  self.txt_file +
        '\n-----------------------------------------------\n' )
        for w, f in self.count_lst:
            # print (w, f) 
            ### add space infront or behind the word to prevent aplabet matching only in word

        
            print ("\n[" + w + '(' + str(f) + ')'+ "]\n ")
            num = 1
            for sen in self.sentence_lst:
                if ' ' + w + ' ' in word_norm(sen): # if word is not single aphabet, just add space infront of word  
                    print (str(num)  + ": "+ sen) 
                    # print (word_norm(sen))
                    num += 1


    ### TODO 1 : return the word with certain frequancy
    ### TODO 2 : sort by alphabet   


    # def show_table2 (self) :
    #     print (self.txt_file)
    #     print ()
    #     print ("---------------------------------------")

    #     for i in range(0,len(self.count_lst)):
    #         print ("---------------------------------------")

    #         print ("word" + str (i+1)  + ":", self.count_lst[i][0], "    freq.: ", self.count_lst[i][1] )
                    
    #         print ("---------------------------------------")

    #         for j in range(0, len(self.sen_group[i])):
    #             print (str(j+1) + ', ', self.sen_group[i][j])

  
if __name__ == "__main__":  
    # file_dir = str(sys.argv[1])
    # for f in os.listdir(pwd + '/' + file_dir+'/'):
    for file_i in os.listdir("src"):
        # tmp_file = file_dir + "/" + f
        file_handle = WordsCounter("src/" + file_i)
    
    # file1 = WordsCounter("src/test docs/doc2.txt")
    # d = file1.word_freq
    # j = file1.json_result
    # lst1 = file1.count_lst
    # count_lst = file1.count_lst

    # print (count_lst[0][0])


    #lst2 = file1.sort_by_freq(lst1)
        # file1.show_table2()
