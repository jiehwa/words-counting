### Words Analysis 
#



### Install

* Install needed python library (nltk)              
`pip install -r requirements.txt`



### How to Use

```
    coding_test 
    |- src  
        |- doc1.txt
        |- doc2.txt
        |- doc3.txt
        |- doc4.txt
    |- main.py
    |- readme.md
```

#### 1. Save the result to a file 

 `python3 main.py  <  [result.txt, file to save the results]`
 

#### 2. Print the result in terminal/ console
`python3 main.py`



### Result 

```
-----------------------------------------------
filename: src/doc1.txt  <FILENAME>
-----------------------------------------------


[the(119)] <WORDS(FREQUANCY)>

<SENTENCES CONTAIN TARGERT WORD...>
 
1: Let me begin by saying thanks to all you who've traveled, from far and wide, to brave the cold today.
2: In the face of war, you believe there can be peace.
3: In the face of despair, you believe there can be hope.
4: In the face of a politics that's shut you out, that's told you to settle, that's divided us for too long, you believe we can be one people, reaching for what's possible, building that more perfect union.
5: That's the journey we're on today.
6: And I accepted the job, sight unseen, motivated then by a single, simple, powerful idea - that I might play a small part in building a better America.
```

## TODO
1. save the data as json for reuse

as example : todo.json
https://github.com/softvar/json2html
2. generate html by json for data visualization




    
